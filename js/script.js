//Функция для наследования
function extend(Child, Parent){
	var F = function() { };
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.prototype.constructor = Child;
	Child.superclass = Parent.prototype;
}

//Класс с функцией для перезапуска
function Restart(){
	//Функция для перезапуска
	this.restart = function(){
		//Скрываем лишние
		$("#action").hide();
		$("#result").hide();
		//Разблокируем кнопку предыдущих этапов
		$("#start").attr('disabled',false);
		$("#generate").attr('disabled',false);
	}
}



//Класс для валидации данных
function Validation(){
	//Проверка на int
	function filterInt(value){
		//Проверяем с помощью регулярного выражения
		if (/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
			//Если число, то вернем значение
			return Number(value);
			//Иначе NaN
		return NaN;
	}

	//Показываем сообщение об ошибке
	function showMsgError(i,val){
		//input и подсказка
		$input=$(".input:eq("+i+")");
		$msg=$(".msgError:eq("+i+")");
		//Красим input
		$input.addClass("error");
		//Меняем текст подсказки и выводим
		$msg.text(val);
		$msg.show();
	}

	//Скрываем сообщение об ошибке
	function hideMsgError(i){
		//input и подсказка
		$input=$(".input:eq("+i+")");
		$msg=$(".msgError:eq("+i+")");
		//Убираем выделение input и скрываем подсказку
		$input.removeClass("error");
		$msg.hide();
	}
	
	//Чистим интерфейс
	function clearInterface(){
		//Скрываем лишние области
		$("#action").hide();
		$("#result").hide();
		//Меняем содержимое
		$("#arrayMsg").text("Набор до сортировки:");
		$("#array").html("");
		$("#resultMsg").text("Набор после сортировки:");
	}
	
	//Проводим валидацию
	this.validation = function(){
		//Обходим поля для ввода
		$(".input").each(function(i){
			//Если не число
			if(isNaN(filterInt($(this).val())))
				showMsgError(i,"должно быть целочисленное значение");
			//Если количество меньше 2
			else if($(this).attr("id")=="count" && +$(this).val()<2)
				showMsgError(i,"должно быть целочисленное значение больше единицы");
			//Если минимум больше, либо равен максимуму
			else if($(this).attr("id")=="max" && +($(this).val()<=+$("#min").val()))
				showMsgError(i,"максимальное значение должно быть больше минимального");
			//Если ошибки нет, но есть сообщение об ошибке от предыдущей валидации
			else if($(this).hasClass("error"))
				hideMsgError(i);
		});
		//Если значения верны
		if(!$(".input").hasClass("error")){
			clearInterface();
			return true;
		}
		//Иначе
		else
			return false;
	}
}

//Класс для генерации массива который будет сортироваться
function GenerateArray(){
	//Массивы для сортируемого массива, анимаций и размер смещения
	this.array = [];
	this.animation=[];
	this.widthItem=50;
	
	//Функция рандомного значения от min до max
	function rand (min, max){
		var rand = min - 0.5 + Math.random() * (max - min + 1)
		rand = Math.round(rand);
		return rand;
	}
	
	function countDigits(n){
		for(var i=0; n>1; i++){
			n /= 10;
		}
		return i;
	}
	
	//Генирируем массив
	this.generatingArray=function(min, max, count){
		//Обнуляем массивы для сортировки и анимации
		this.array = [];
		this.animation=[];
		//Если числа слишком большие меняем смещение
		if(max>99999){
			//Если число настолько большое, что будет сокращенная запись числа
			if(countDigits(max)>21)
				this.widthItem=210;
			//Иначе
			else
				this.widthItem=countDigits(max)*10;
		}
		//Генерируем count чисел
		for (var i=0; i<count; i++){
			var value=rand(min, max);
			//Записываем в массив
			this.array.push(value);
			//Выводим на экран в двух местах
			$("#arrayMsg").text($("#arrayMsg").text()+' '+value+';');
			$("<div>").addClass('item').text(value).appendTo($("#array"));
		}
		//Устанавливаем ширину ячеек
		$(".item").css("width",this.widthItem-2);
		//Устанавливаем длину контейнера в зависимости от количества элементов
		$("#array").css("min-width",this.widthItem*count);
		//Показываем результат
		$("#action").show();
	}
}

//Класс для сортировки
function BubbleSort(){
	//Продолжительность анимаций
	this.swapTime=500;
	this.testTime=1000;
	this.closeTime=0;
	//Для приватных функциий
	var self = this;
	
	//вызов родительского конструктора
	BubbleSort.superclass.constructor.call(this);
	
	//Шаг при переборе анимаций (первая анимация в списке)
	function step(){
		//Если меняем местами, то вызываем две анимации для элементов
		if(self.animation[0].animation=="swap"){
			swap(self.animation[0].firstEl, self.animation[0].secondEl);
			swap(self.animation[0].secondEl, self.animation[0].firstEl);
		}
		//Если просто проверяем, без замены, то подсвечиваем
		else if(self.animation[0].animation=="test")
			test(self.animation[0].firstEl, self.animation[0].secondEl);
		//Если элемент определен и далее не будет изменяться
		else if(self.animation[0].animation=="close")
			close(self.animation[0].el);
	}
	
	//Меняем элементы местами
	function swap(i,j){
		//Если анимация для левого элемента
		if(i<j)
			to=self.widthItem;
		//Если анимация для правого элемента 
		else
			to=-self.widthItem;
		//Новое значение для элемента (тот который встанет на данное место)
		var value=$(".item:eq("+j+")").text();
		//Передвигаемый элемент
		$elem=$(".item:eq("+i+")");
		//Меняем цвет
		$elem.css("background-color","#ffff00");
		//Запускаем анимацию, сначала сдвигаем вверх/вниз, потом ставим на место другого
		$elem.animate({
				top: -to/2,
				left: to/2,
		}, self.swapTime).animate({
				top: 0,
				left: to,
		}, self.swapTime, function(){
			//Когда анимация завершена ставим элемент на место и устанавливаем значение другого элемента
			$(this).text(value),
			//Возвращаем стандартный цвет
			$(this).css("background-color", "#fff");
			$(this).css("left", "0px");
			//Вызываем завершение шага (вешается только на одну анимацию)
			if(i>j)
				endStep();
		});
	}
	
	//Просто проверка значения, без замены
	function test(i,j){
		//Получаем элементы
		$elem1=$(".item:eq("+i+")");
		$elem2=$(".item:eq("+j+")");
		//Меняем их цвета
		$elem1.css("background-color","#cceecc");
		$elem2.css("background-color","#cceecc");
		//Возвращаем стандартный цвет и вызываем завершение шага
		setTimeout(function(){
			$elem1.css("background-color","#fff");
			$elem2.css("background-color","#fff");
			endStep();
		}, self.testTime);
	}
	
	//Красим элемент выпавший из сортировки
	function close(i){
		//Выбираем элемент
		$elem=$(".item:eq("+i+")");
		//Красим
		$elem.css("background-color","#c0c0c0");
		//Вызываем завершение шага
		setTimeout(function(){
			endStep();
		}, self.closeTime);
	}
	
	//Завершение шага после анимаций
	function endStep(){
		//Удаляем отработанный элемент
		self.animation.shift();
		//Если еще есть записи об анимации вызываем следующий шаг
		if(self.animation.length>0)
			step();
		//Иначе выводим массив и показываем блок с результатом
		else{
			for(var i=0, l=self.array.length; i<l; i++){
				$("#resultMsg").text($("#resultMsg").text()+' '+self.array[i]+';');
				$("#result").show();
			}
		}
	}
	
	//Запоминаем анимацию
	function rememberAnimation(firstEl,animation){
		//Создаем объект с данными об анимации и возвращаем
		var obj = {};
		//Если убираем определившийся элемент из сортировки
		if(animation=="close")
			obj.el=firstEl;
		//Если иная анимация
		else{
			obj.firstEl=firstEl;
			obj.secondEl=firstEl+1;
		}
		obj.animation=animation;
		return obj;
	}
	
	//Сортируем массив
	this.sort = function(){
		//Блокируем кнопки
		$("#start").attr('disabled',true);
		$("#generate").attr('disabled',true);
		// Обходим массив (получаем данные об анимации и получаем итоговый массив)
		for (var i=1, l=this.array.length; i<=l; i++){
			for (var j=0; j<l-i; j++){
				//Если элементы необходимо поменять местами
				if(this.array[j]>this.array[j+1]){
					this.animation.push(rememberAnimation(j,"swap"));
					var tmp=this.array[j];
					this.array[j]=this.array[j+1];
					this.array[j+1]=tmp;
				}
				//Если элементы не нужно менять местами
				else{
					this.animation.push(rememberAnimation(j,"test"));
				}
			}
			//Помечаем отбрасываемый элемент
			this.animation.push(rememberAnimation(l-i,"close"));
		}
		//Запускаем анимацию на основе сформированных данных
		step();
	}
}

//устанавливаем наследование
extend(BubbleSort, GenerateArray);

var bubbleSort = new BubbleSort();
var validation = new Validation();
var restart = new Restart();

//Запуск генерации
$("#generate").click(function(){
	//Если значения валидны
	if(validation.validation()){
		bubbleSort.generatingArray($("#min").val(),$("#max").val(),$("#count").val());
	}
});

//Запусаем сортировку
$("#start").click(function(){
	bubbleSort.sort();
});

//Сбрасываем результат и начинаем заного
$("#restart").click(function(){
	restart.restart();
});