Логика приложения следующая.

Сначала сортируется массив и все данные о шагах сортировки запоминаются в массив объектов для анимации. Когда сортировка завершена, начинается анимация. Это позволяет сделать следующие: полностью отсортировать массив, а потом не обращая внимание на предыдущий или следующий шаг запускать анимацию.

После завершения анимации перестановки элементов вызывается callback функция, которая завершает шаг, и вызывает следующий. Это позволяет запускать анимацию последовательно. Остальные эффекты написаны с помощью setTimeout, так как смена цветы фона в анимации не делается. 

Решение использовать animate от jquery, вместо анимации на css связано именно с упорядочиванием анимации друг за другом, без лишних вычислений и снижением вероятности рассинхронизации логики и интерфейса.Также подобное решение позволяет поддерживать большее количество браузеров (в частности ie от 8 версии, а не от 10).

Костыль: на js выполняется установка ширины графического контейнера и его элементов (отображение процесса сортировки). Это связано с тем, что не получилось сделать резиновые по ширине блоки в одну строку.